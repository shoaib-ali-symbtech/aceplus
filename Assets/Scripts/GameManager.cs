using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackHandler();
        }
    }

    private void BackHandler()
    {
        var sceneManager = SceneManager.GetActiveScene();
        if (sceneManager.name=="SplashScreen" || sceneManager.name == "HomeScene")
               Application.Quit();
        else
        {
                if (sceneManager.buildIndex > 4)
            {
                SceneManager.LoadScene("HomeScene");
            }
            else
            {
                SceneManager.LoadScene(0);
            }
        }
    }


    public void GotoScene(string sceneName)
    {
      

        SceneManager.LoadScene(sceneName);

    }
    public void GotoScene(int buildIndex)
    {
       
        SceneManager.LoadScene(buildIndex);
    }


    
}
